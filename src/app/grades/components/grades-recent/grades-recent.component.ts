import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {CourseGradeBase, GradeAverageResult, GradesService} from '../../services/grades.service';
import {ConfigurationService, ErrorService, GradeScale, GradeScaleService, LocalizedAttributesPipe, ModalService} from '@universis/common';
import { LoadingService } from '@universis/common';
import { ChartOptions } from 'chart.js';
import {ModalModule} from 'ngx-bootstrap';
import {CourseGradeDistributionBase} from '../../services/grades.service';


@Component({
  selector: 'app-grades-recent',
  templateUrl: './grades-recent.component.html',
  styleUrls: ['./grades-recent.component.scss']
})
export class GradesRecentComponent implements OnInit {

  public latestCourses: any = [];
  public isCollapsed: boolean[];
  public isLoading = true;


  /* for stats box */
  public simpleAverageObj: GradeAverageResult = {
    courses: 0,
    coefficients: 0,
    average: 0,
    ects: 0,
    units: 0,
    passed: 0,
    grades: 0,
  };
  public defaultGradeScale: GradeScale;
  private courseTeachers: any;
  private latestExamPeriod: string;

  /* for stats box */
  public registeredCourseCount: number;
  public passedCourseCount: number;
  public failedCourseCount: number;
  public passedGradeAverage: string;

  /* modal values */
  @ViewChild('modalTemplate') modalTemplate: TemplateRef<any>;
  public modalRef: any;
  public modalCourse: CourseGradeDistributionBase;

  public defaultLanguage = null;
  public currentLanguage = null;

  public pendingGrades: any[];

  constructor(private gradesService: GradesService,
              private loadingService: LoadingService,
              private modal: ModalService,
              private gradeScaleService: GradeScaleService,
              private  errorService: ErrorService,
              private _configurationService: ConfigurationService,
              private localizePipe: LocalizedAttributesPipe) {
    this.currentLanguage = this._configurationService.currentLocale;
    this.defaultLanguage = this._configurationService.settings.i18n.defaultLocale;
  }

  ngOnInit() {
    // show loading
    this.loadingService.showLoading();

    this.gradesService.getRecentGrades().then((res) => {
      // get default grade scale
      return this.gradesService.getDefaultGradeScale().then(gradeScale => {
        // set grade scale
        this.defaultGradeScale = gradeScale;
        this.latestCourses = res;
        this.latestCourses.sort((a, b) => a.course.name.localeCompare(b.course.name));
        this.isCollapsed = Array(this.latestCourses.length).fill(true);
        const gradeGroup: Array<CourseGradeBase> = [];

        this.latestCourses.forEach(course => {
          const courseGradeBaseObj: CourseGradeBase = {
            courseStructureType: 1,
            units: course.course.units,
            calculateGrade:  course.course && course.course.gradeScale &&  course.course.gradeScale.scaleType !== 3,
            calculateUnits: true,
            isPassed: course.isPassed,
            ects: course.course.ects,
            coefficient: 1,
            grade: course.grade1
          };
          gradeGroup.push(courseGradeBaseObj);
        });

        // set average of passed courses
        this.simpleAverageObj = this.gradesService.getGradesSimpleAverage(gradeGroup);
        if (this.simpleAverageObj) {
          // set passed courses
          this.passedCourseCount = this.simpleAverageObj.passed;
          // set failed courses
          this.failedCourseCount = this.simpleAverageObj.courses - this.simpleAverageObj.passed;
          // set sum of courses
          this.registeredCourseCount = this.simpleAverageObj.courses;
          // set Simple average of passed courses
          this.passedGradeAverage = this.defaultGradeScale.format(this.simpleAverageObj.average);
        }
        this.gradesService.getPendingGrades().then((pendingGrades) => {
          this.pendingGrades = pendingGrades;
          // hide loading
          this.loadingService.hideLoading();
          this.isLoading = false;
        }).catch(err => {
          // TODO: handle error
          console.error(err);
        });
      }).catch(err => {
        // TODO: handle error
        console.error(err);
      });
    }).catch(err => {
      // TODO: handle error
      console.error(err);
    });
  }

  showStatistics(course) {
    try {
      this.modalCourse = {
        name: this.localizePipe.transform(course.course, 'name'),
        displayCode: course.course.displayCode,
        examPeriodName: course.courseExam.examPeriod.name,
        examPeriodYear: course.courseExam.year.alternateName,
        gradeScaleId: course.course.gradeScale.id,
        examId: course.courseExam.id
      }
      this.modalRef = this.modal.openModal(this.modalTemplate, 'modal-lg');
    } catch (err) {
      console.log(err)
      return this.errorService.navigateToError(err);
    }
  }

  closeModal() {
    this.modalCourse = {
      name:'',
      examPeriodName: '',
      examPeriodYear: '',
      examId: 0,
      displayCode: '',
      gradeScaleId: 0
    };
    this.modalRef.hide();
  }

}
