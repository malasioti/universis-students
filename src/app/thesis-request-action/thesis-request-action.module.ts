import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThesisRequestActionRoutingModule } from './thesis-request-action-routing.module';
import { ApplyComponent } from './apply/apply.component';
import { PreviewComponent } from './preview/preview.component';
import { FormsModule } from '@angular/forms';
import { MostModule } from '@themost/angular';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AdvancedFormsModule } from '@universis/forms';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MostModule,
    SharedModule,
    TranslateModule,
    AdvancedFormsModule,
    ThesisRequestActionRoutingModule
  ],
  declarations: [ApplyComponent, PreviewComponent]
})
export class ThesisRequestActionModule {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch( err => {
      console.error('An error occurred while loading ThesisRequestActionModule.');
      console.error(err);
    });
  }

  // tslint:disable-next-line: use-life-cycle-interface
  async ngOnInit() {
    const sources = environment.languages.map(async (language) => {
      const translations = await import(`./i18n/thesis-request-action.${language}.json`);
      this._translateService.setTranslation(language, translations, true);
    });
    // execute chain
    await Promise.all(sources);
  }

 }
