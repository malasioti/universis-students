import {DiningModule} from '@universis/ngx-dining';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [DiningModule]
})
export class DiningWrapperModule {
}
