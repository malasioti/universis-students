import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoRequestComponent } from './auto-request.component';
import {FormsModule} from '@angular/forms';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ModalModule, TooltipModule} from 'ngx-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import {TranslateModule} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {ConfigurationService, ErrorService, LoadingService, ModalService, SharedModule, UserService} from '@universis/common';
import {TestingConfigurationService} from '../../../test';
import {RequestsService} from '../../services/requests.service';
import {ProfileService} from '../../../profile/services/profile.service';
import {MessageSharedService} from '../../../students-shared/services/messages.service';

describe('AutoRequestComponent', () => {
  let component: AutoRequestComponent;
  let fixture: ComponentFixture<AutoRequestComponent>;

  const requestsSvc = jasmine.createSpyObj('RequestsService' , ['getDocumentTypes', 'getDocumentRequest',
    'getReportTemplates']);
  const requestedTypes = requestsSvc.getDocumentTypes.and.returnValue(Promise.resolve(JSON.parse('[]')));
  const availableTemplates = requestsSvc.getReportTemplates.and.returnValue(Promise.resolve(JSON.parse('[]')));
  const modalSvc = jasmine.createSpyObj('ModalService' , ['showDialog', 'openModal']);
  const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);
  const profileSvc = jasmine.createSpyObj('ProfileService', ['getStudent']);
  const userSvc = jasmine.createSpyObj('UserService' , ['getUser', 'getProfile', 'checkLogin']);
  const messageSvc = jasmine.createSpyObj('MessageSharedService', ['callUnReadMessages']);
  const errorSvc = jasmine.createSpyObj('ErrorService', ['navigateToError', 'showError']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoRequestComponent ],
      imports: [
        InfiniteScrollModule,
        RouterTestingModule,
        HttpClientTestingModule,
        ModalModule.forRoot(),
        TranslateModule.forRoot(),
        TooltipModule.forRoot(),
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        SharedModule
      ],
      providers: [
        {
          provide: RequestsService,
          useValue: requestsSvc
        },
        {
          provide: ModalService,
          useValue: modalSvc
        },
        {
          provide: LoadingService,
          useValue: loadingSvc
        },
        {
          provide: ProfileService,
          useValue: profileSvc
        },
        {
          provide: UserService,
          useValue: userSvc
        },
        {
          provide: MessageSharedService,
          useValue: messageSvc
        },
        {
          provide: ErrorService,
          useValue: errorSvc
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
