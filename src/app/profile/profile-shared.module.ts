import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {MostModule} from '@themost/angular';
import {environment} from '../../environments/environment';
import {ProfileCardComponent} from './components/profile-card/profile-card.component';
import {SharedModule} from '@universis/common';
import {ProfileService} from './services/profile.service';
import {StudentsSharedModule} from '../students-shared/students-shared.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        MostModule,
        StudentsSharedModule
    ],
    declarations: [
        ProfileCardComponent
    ],
    exports: [
        ProfileCardComponent
    ],
    providers: [
        ProfileService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProfileSharedModule {

    constructor(private _translateService: TranslateService) {
        environment.languages.forEach((culture) => {
            import(`./i18n/profile.${culture}.json`).then((translations) => {
                this._translateService.setTranslation(culture, translations, true);
            });
        });
    }
}
